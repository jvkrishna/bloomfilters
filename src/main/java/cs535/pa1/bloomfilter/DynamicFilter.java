package cs535.pa1.bloomfilter;

import java.util.LinkedList;

/**
 * Dynamic Bloom Filter Implementation using random hash function.
 * 
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 */
public class DynamicFilter {

	private int initialSize = 1000;
	private int bitsPerElement;
	private int addedElements;
	private LinkedList<BloomFilterRan> bloomFilters = new LinkedList<>();

	public DynamicFilter(int bitsPerElement) {
		this.bitsPerElement = bitsPerElement;
		bloomFilters.add(new BloomFilterRan(initialSize, bitsPerElement));
	}

	public void add(String s) {
		if (addedElements >= initialSize * (Math.pow(2, bloomFilters.size()) - 1)) {
			bloomFilters.add(new BloomFilterRan(2 * addedElements, bitsPerElement));
		}
		addedElements++;
		bloomFilters.peekLast().add(s);
	}

	public boolean appears(String s) {
		return bloomFilters.stream().anyMatch(bloomFilter -> bloomFilter.appears(s));
	}

	public int filterSize() {
		return bloomFilters.peekLast().filterSize();
	}

	public int dataSize() {
		return addedElements;
	}

	public int numHashes() {
		return bloomFilters.peekLast().numHashes();
	}

}