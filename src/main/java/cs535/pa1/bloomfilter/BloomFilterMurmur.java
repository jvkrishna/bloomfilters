package cs535.pa1.bloomfilter;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

/**
 * Bloom Filter Implementation using deterministic murmur hash function.
 * 
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 */
public class BloomFilterMurmur {

	private int filterSize;
	private int addedElements;
	private BitSet bloomFilter;
	private int hashFunctions;

	public BloomFilterMurmur(int setSize, int bitsPerElement) {
		filterSize = setSize * bitsPerElement;
		bloomFilter = new BitSet(filterSize);
		hashFunctions = (int) (Math.log(2) * filterSize / setSize);
	}

	public void add(String s) {
		addedElements++;
		getIndices(s).forEach(index -> {
			bloomFilter.set(index);
		});
	}

	public boolean appears(String s) {
		return getIndices(s).stream().allMatch(index -> bloomFilter.get(index));
	}

	public int filterSize() {
		return filterSize;
	}

	public int dataSize() {
		return addedElements;
	}

	public int numHashes() {
		return hashFunctions;
	}

	private List<Integer> getIndices(String s) {
		List<Integer> indices = new ArrayList<>();
		for (int i = 1; i <= hashFunctions; i++) {
			indices.add((int) Math.abs((murmurHash(s, 31 << i)) % filterSize));
		}
		return indices;

	}

	private long murmurHash(String s, int seed) {
		s = s.toLowerCase();
		return MurmurHash.hash64(s.getBytes(), s.length(), seed);
	}

}