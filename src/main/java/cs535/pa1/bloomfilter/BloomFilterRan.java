package cs535.pa1.bloomfilter;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Bloom Filter Implementation using random hash function.
 * 
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 */
public class BloomFilterRan {

	private int filterSize;
	private int addedElements;
	private BitSet bloomFilter;
	private int hashFunctions;
	private int[] aArr, bArr;

	public BloomFilterRan(int setSize, int bitsPerElement) {
		filterSize = getNextPrime(setSize * bitsPerElement);
		bloomFilter = new BitSet(filterSize);
		hashFunctions = (int) (Math.log(2) * filterSize / setSize);
		aArr = new int[hashFunctions];
		bArr = new int[hashFunctions];
		for (int i = 0; i < hashFunctions; i++) {
			aArr[i] = ThreadLocalRandom.current().nextInt(0, filterSize);
			bArr[i] = ThreadLocalRandom.current().nextInt(0, filterSize);
		}
	}

	public void add(String s) {
		addedElements++;
		getIndices(s).forEach(index -> {
			bloomFilter.set(index);
		});
	}

	public boolean appears(String s) {
		return getIndices(s).stream().allMatch(index -> bloomFilter.get(index));
	}

	public int filterSize() {
		return filterSize;
	}

	public int dataSize() {
		return addedElements;
	}

	public int numHashes() {
		return hashFunctions;
	}

	private List<Integer> getIndices(String s) {
		s = s.toLowerCase();
		List<Integer> indices = new ArrayList<>();
		for (int i = 1; i <= hashFunctions; i++) {
			indices.add(Math.abs(randomHash(s, i - 1) % filterSize));
		}
		return indices;

	}

	private int randomHash(String s, int i) {
		return (aArr[i] * s.hashCode()) + bArr[i];
	}

	private int getNextPrime(int start) {
		// We are guaranteed to get a prime between start and 2*start
		if (start % 2 == 0)
			start++;
		for (int i = start; i < 2 * start; i = i + 2) {
			if (isPrime(i))
				return i;
		}
		throw new IllegalArgumentException("Unable to find the next prime number.");
	}

	private boolean isPrime(int num) {
		if (num % 2 == 0 || num % 3 == 0)
			return false;
		int i = 5;
		while (i * i < num) {
			if (num % i == 0 || num % (i + 2) == 0)
				return false;
			i += 6;
		}
		return true;
	}


}