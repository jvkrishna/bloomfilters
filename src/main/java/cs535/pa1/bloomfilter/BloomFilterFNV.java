package cs535.pa1.bloomfilter;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

/**
 * Bloom Filter Implementation using deterministic FNV-64 hash function.
 * 
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 */
public class BloomFilterFNV {

	private static final Long FNV64PRIME = 109951168211L;
	private static final BigInteger FNV64_INIT = new BigInteger("14695981039346656037");

	private int filterSize;
	private int addedElements;
	private BitSet bloomFilter;
	private int hashFunctions;

	public BloomFilterFNV(int setSize, int bitsPerElement) {
		filterSize = setSize * bitsPerElement;
		bloomFilter = new BitSet(filterSize);
		hashFunctions = (int) (Math.log(2) * filterSize / setSize);
	}

	public void add(String s) {
		addedElements++;
		getIndices(s).forEach(index -> {
			bloomFilter.set(index);
		});
	}

	public boolean appears(String s) {
		return getIndices(s).stream().allMatch(index -> bloomFilter.get(index));
	}

	public int filterSize() {
		return filterSize;
	}

	public int dataSize() {
		return addedElements;
	}

	public int numHashes() {
		return hashFunctions;
	}

	private List<Integer> getIndices(String s) {
		s = s.toLowerCase();
		List<Integer> indices = new ArrayList<>();
		BigInteger hash1 = fnv_1a(s);
		int hash2 = s.hashCode();
		for (int i = 1; i <= hashFunctions; i++) {
			hash2 >>= i;
			// index value calculated as ((hash1*i)+hash2)%filterSize
			indices.add(hash1.multiply(BigInteger.valueOf(i)).add(BigInteger.valueOf(hash2))
					.mod(BigInteger.valueOf(filterSize)).intValue());
		}
		return indices;
	}

	private BigInteger fnv_1a(String s) {
		BigInteger hash = FNV64_INIT;
		for (byte b : s.getBytes()) {
			hash = hash.xor(BigInteger.valueOf(b));
			hash = (hash.multiply(BigInteger.valueOf(FNV64PRIME))).mod(new BigInteger("1").shiftLeft(64));
		}
		return hash;
	}

}
