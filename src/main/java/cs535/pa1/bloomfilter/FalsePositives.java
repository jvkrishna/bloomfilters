package cs535.pa1.bloomfilter;

import java.util.HashSet;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 */
public class FalsePositives {

	
	private static final int ADD_NUMBER = 500_000;
	private static final int TEST_NUMBER = 500_000;
	private static final int BITS_PER_ELEMENT = 16;
	private static final double ideal = Math.pow(0.618, BITS_PER_ELEMENT);
	private static Set<String> addDataSet;
	private static Set<String> testDataSet;
	private static RandomString randomString = new FalsePositives().new RandomString();

	static {
		prepareDataSet(ADD_NUMBER, TEST_NUMBER);
	}
	
	public void testFNVFalsePositives() {
		BloomFilterFNV bffnv = new BloomFilterFNV(ADD_NUMBER, BITS_PER_ELEMENT);
		
		long time = System.currentTimeMillis();
		addDataSet.forEach(s -> bffnv.add(s));
		System.out.println("Time to add single element using FNV "+ (System.currentTimeMillis() - time)/(double) ADD_NUMBER);
		
		int falsePositives = 0;
		time = System.currentTimeMillis();
		for (String s : testDataSet) {
			if (bffnv.appears(s))
				falsePositives++;
		}
		System.out.println("Time to search single element using FNV " + (System.currentTimeMillis() - time)/(double) TEST_NUMBER);
		
		System.out.println("FNV False Positives: " + (double) falsePositives/TEST_NUMBER);
		
		calculate((double) falsePositives/TEST_NUMBER);
	}
	
	public void calculate(double a)
	{
		System.out.println("Percentage Deviation From Ideal Value " + ((a-ideal)/ideal)*100);
	}
	
	public void testMurmurFalsePositives() {
		BloomFilterMurmur bffnv = new BloomFilterMurmur(ADD_NUMBER, BITS_PER_ELEMENT);
		
		long time = System.currentTimeMillis();
		addDataSet.forEach(s -> bffnv.add(s));
		System.out.println("Time taken to add a single element using Murmur Hash " + (System.currentTimeMillis() - time)/(double) ADD_NUMBER);
		
		int falsePositives = 0;
		time = System.currentTimeMillis();
		for (String s : testDataSet) {
			if (bffnv.appears(s))
				falsePositives++;
		}
		System.out.println("Time taken to search a single element using Murmur Hash " +(System.currentTimeMillis() - time)/(double) TEST_NUMBER);
		
		System.out.println("Murmur False Positives: " + (double) falsePositives/TEST_NUMBER);
		
		calculate((double) falsePositives/TEST_NUMBER);
	}
	
	public void testRandomFalsePositives() {
		BloomFilterRan bffnv = new BloomFilterRan(ADD_NUMBER, BITS_PER_ELEMENT);
		long time = System.currentTimeMillis();
		addDataSet.forEach(s -> bffnv.add(s));
		System.out.println("Time taken to add a single element using Random Hash " + (System.currentTimeMillis() - time)/(double) ADD_NUMBER);
		
		int falsePositives = 0;
		time = System.currentTimeMillis();
		for (String s : testDataSet) {
			if (bffnv.appears(s))
				falsePositives++;
		}
		System.out.println("Time taken to search for a single element using Random Hash " + (System.currentTimeMillis() - time)/(double) TEST_NUMBER);
		System.out.println("Random False Positives: " + (double) falsePositives/TEST_NUMBER);
		calculate((double) falsePositives/TEST_NUMBER);
	}
	
	public void testDynamicFilterFalsePositives() {
		DynamicFilter bffnv = new DynamicFilter(BITS_PER_ELEMENT);
		long time = System.currentTimeMillis();
		addDataSet.forEach(s -> bffnv.add(s));
		System.out.println("Time taken to add a single element using Dynamic Filters " + (System.currentTimeMillis() - time)/(double) ADD_NUMBER);
		
		int falsePositives = 0;
		time = System.currentTimeMillis();
		for (String s : testDataSet) {
			if (bffnv.appears(s))
				falsePositives++;
		}
		System.out.println("Time taken to search a single element using Dynamic Filters " + (System.currentTimeMillis() - time)/(double) TEST_NUMBER);
		System.out.println("Dynamic Filter False Positives: " + (double) falsePositives/TEST_NUMBER);
	}
	

	private static void prepareDataSet(int addNo, int testNo) {
		addDataSet = new HashSet<>(addNo);
		testDataSet = new HashSet<>(testNo);
		
		while (addDataSet.size() < addNo) {
			addDataSet.add(randomString.nextString());
		}
		
		while (testDataSet.size() < testNo) {
			String str = randomString.nextString();
			if (!addDataSet.contains(str)) {
				testDataSet.add(str);
			}
		}
		
	}

	private class RandomString {

		private final Random random;
		private final char[] symbols;
		private final char[] buf;

		public RandomString() {
			this(8);

		}

		public RandomString(int lenght) {
			this(lenght, "abcdefghijklmnopqustuvwzyz0123456789");

		}

		public RandomString(int lenght, String symbols) {
			this.buf = new char[lenght];
			this.symbols = symbols.toCharArray();
			this.random = Objects.requireNonNull(ThreadLocalRandom.current());
		}

		public String nextString() {
			for (int i = 0; i < buf.length; i++) {
				buf[i] = symbols[random.nextInt(symbols.length)];
			}
			return new String(buf);
		}

	}

	public static void main(String[] args) {
		FalsePositives fp = new FalsePositives();
		
		long time = System.currentTimeMillis();
		fp.testDynamicFilterFalsePositives();
		System.out.println("Total time taken to Run " + (System.currentTimeMillis() - time) + " milliseconds");
		
		time = System.currentTimeMillis();
		fp.testFNVFalsePositives();
		System.out.println("Total time taken to Run " + (System.currentTimeMillis() - time) + " milliseconds");
		
		time = System.currentTimeMillis();
		fp.testMurmurFalsePositives();
		System.out.println("Total time taken to Run " + (System.currentTimeMillis() - time) + " milliseconds");
		
		time = System.currentTimeMillis();
		fp.testRandomFalsePositives();
		System.out.println("Total time taken to Run " + (System.currentTimeMillis() - time) + " milliseconds");
	}

}
