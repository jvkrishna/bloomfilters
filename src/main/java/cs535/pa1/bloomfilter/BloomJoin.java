package cs535.pa1.bloomfilter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

public class BloomJoin {

	private File Relation1;
	private File Relation2;

	public BloomJoin(String r1, String r2) {
		this.Relation1 = new File(r1);
		this.Relation2 = new File(r2);
	}

	public void join(String r3) {
		BloomFilterMurmur filter = new BloomFilterMurmur(2000000, 8);

		try {
			String line = "";
			BufferedReader br1 = new BufferedReader(new FileReader(this.Relation1));

			// Add join attribute to the filter.
			while ((line = br1.readLine()) != null) {
				String[] split = line.split("   ");
				filter.add(split[0]);
			}
			br1.close();

			BufferedReader br2 = new BufferedReader(new FileReader(this.Relation2));
			Map<String, String> r3Map = new HashMap<>();

			// Create a map using the matching join attributes.
			while ((line = br2.readLine()) != null) {
				String[] newSplit = line.split("   ");

				if (filter.appears(newSplit[0])) {
					r3Map.put(newSplit[0], newSplit[1]);
				}
			}
			br2.close();

			// Creates the join file using the R1 and the R3 Map
			try (BufferedReader br = new BufferedReader(new FileReader(this.Relation1));
					BufferedWriter bw = new BufferedWriter(new FileWriter(r3));) {
				while ((line = br.readLine()) != null) {
					String[] split = line.split(" +");
					if (r3Map.get(split[0]) != null) {
						bw.write(line + "\t" + r3Map.get(split[0]));
						bw.newLine();
					}

				}
				br.close();
				bw.close();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void main(String args[])
	{
		BloomJoin join = new BloomJoin("/Users/revanth/Downloads/Relation1.txt", "/Users/revanth/Downloads/Relation2.txt");

		join.join("/Users/revanth/Downloads/Relation3.txt");
	}
}
